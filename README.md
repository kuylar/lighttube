> MOVED TO GITHUB
> https://github.com/kuylar/lighttube

# lighttube - A lightweight, privacy respecting alternative frontend for YouTube

---

## Features

- Lightweight
- Ad-free
- Dislike counts from [Return YouTube Dislike](https://www.returnyoutubedislike.com/)
- Subscription feed
- Proxying the videos through LightTube

---

## Screenshots

### Desktop

| Page | Light | Dark |
| :--- | :---: | :--: |
| Search | ![image](https://gitlab.com/kuylar/lighttube/-/raw/master/screenshots/desktop/light/search.png?inline=false)| ![image](https://gitlab.com/kuylar/lighttube/-/raw/master/screenshots/desktop/dark/search.png?inline=false) |
| Video | ![image](https://gitlab.com/kuylar/lighttube/-/raw/master/screenshots/desktop/light/video.png?inline=false)| ![image](https://gitlab.com/kuylar/lighttube/-/raw/master/screenshots/desktop/dark/video.png?inline=false) |

### Mobile

| Page | Light | Dark |
| :--- | :---: | :--: |
| Search | ![image](https://gitlab.com/kuylar/lighttube/-/raw/master/screenshots/mobile/light/search.png?inline=false)| ![image](https://gitlab.com/kuylar/lighttube/-/raw/master/screenshots/mobile/dark/search.png?inline=false) |
| Video | ![image](https://gitlab.com/kuylar/lighttube/-/raw/master/screenshots/mobile/light/video.png?inline=false)| ![image](https://gitlab.com/kuylar/lighttube/-/raw/master/screenshots/mobile/dark/video.png?inline=false) |

---

[XML API Documentation](https://gitlab.com/kuylar/lighttube/-/wikis/XML-API) - [Used libraries](https://gitlab.com/kuylar/lighttube/-/blob/master/OTHERLIBS.md)  - [Public instances](https://gitlab.com/kuylar/lighttube/-/blob/master/INSTANCES.md) 
